/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam.implementations;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zavya
 */
public class BoardImp implements Board {

    private final int row = 3;
    private final int col = 3;
    PlayerType[][] board = new PlayerType[row][col];

    public BoardImp() {
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                board[i][j] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        //metódussal le tudjuk kérdezni a tábla rowIdx-edik sorának colIdx-edik elemét. 
        //Figyelj arra is hogy a metódus exception-t dobjon, ha a kérdezett sor vagy oszlop nincs a táblán. 
        if (colIdx < 0 || colIdx >= board.length || rowIdx < 0 || rowIdx >= board[0].length) {
            throw new CellException(rowIdx, colIdx, "Táblán kívüli terület!");
        }
        return board[colIdx][rowIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {
        if (cell.getCol() < 0 || cell.getCol() >= board.length || cell.getRow() < 0 || cell.getRow() >= board[0].length) {
            throw new CellException(cell.getRow(), cell.getCol(), "Nem jó helyre tennél, próbáld újra!");
        }
        if (board[cell.getCol()][cell.getRow()] != PlayerType.EMPTY) {
            throw new CellException(cell.getRow(), cell.getCol(), "Nem jó helyre tennél vagy foglalt! Próbáld újra!");
        }
        board[cell.getCol()][cell.getRow()] = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {
        for (int i = 0; i < col; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] == p) {
                return true;
            }
        }
        for (int i = 0; i < row; i++) {
            if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] == p) {
                return true;
            }
        }
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] == p) {
            return true;
        }
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] == p) {
            return true;
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptyList = new ArrayList<>();
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                if (board[i][j] == PlayerType.EMPTY) {
                    emptyList.add(new Cell(j, i, PlayerType.EMPTY));
                }
            }
        }
        return emptyList;
    }
}
